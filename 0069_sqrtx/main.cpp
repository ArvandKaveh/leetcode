#include <iostream>
#include <vector>
#include <cassert>
#include <bitset>
#include <cstdint>

#include "../0000_utils/io.h"

// TODO - This solution was bad :)
// Solution1
int mySqrt(int x) {

    int_fast8_t x0 = x >> 1;

    int it1 = 0;
    while (x0 * x0 > x) {
        x0 >>= 2;
        ++it1;
    }

    std::cout << "iteration 1: " << it1 << "x = " << x0 << '\n';

    int it2 = 0;
    while (x0 * x0 <= x) {
        ++x0;
        ++it2;
    }

    std::cout << "iteration 2: " << it2 << "x = " << x0 << '\n';

    return static_cast<int>(x0 - 1);
 }


int main () {

    // Get the inputs example inputs
    std::vector<int> inputs {4, 8, 16, 32, 65536, 33554432};
    int answers[] {2, 2, 4, 5, 256, 5792};

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        int answer = mySqrt(inputs[i]);
        std::cout << "Sqrt(" <<inputs[i] << ") = " << answer
                  << '\n';

    assert(answer == answers[i]);
    }

    return 0;
}
