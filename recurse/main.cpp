#include <iostream>
#include <string>

void recurse1(int i) {
    std::cout << i << '\n';
    recurse1(++i);
}

void recurse2(int i, std::string s1, std::string s2) {
    std::cout << i << '\n';
    recurse2(++i, s1 + "a", s2 + "a");
}

int main () {

    // recurse1(1);

    recurse2(1,"a","a");
    
    return 0;
}
