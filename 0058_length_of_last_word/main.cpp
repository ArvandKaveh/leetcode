#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include "../0000_utils/io.h"

// Solution1

int lengthOfLastWord(std::string s) {

    if (s.empty())
        return 0;

    // get the index of the first appeareance of the trainling spaces (get rid of the trailing spaces)
    int i = s.size();
    while (i > 0 && s[i-1] == ' ') {
        --i;
    }

    // case all spaces
    if (i == 0)
        return 0;

    // find the index of last space, which is not a trailing space
    // return it's distance to the last index of s withouth the trailing spaces
    int j = i - 1; // i: new size of the s
    while (j >= 0) {
        if (s[j] == ' ')
            return i - j - 1;
        
        --j;
    }

    return i;
 }


int main () {

    // Get the inputs example inputs
    std::vector<std::string> inputs {
        "Hello World",  // 5
        " ",            // 0
        "",             // 0
        "aaa",          // 3
        "a ",           // 1
        "a   ",         // 1
        "aa a",         // 1
        "aa a a  ",     // 1
        "aa a aaaaa  ", // 5
        "     "         // 0
    };

    std::vector<int> answers {5, 0, 0, 3, 1, 1, 1, 1, 5, 0};

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        std::cout << "word: \"" <<inputs[i] << "\", size: " << inputs[i].size() << '\n'
                  << lengthOfLastWord(inputs[i])
                  << "\n";

    assert(lengthOfLastWord(inputs[i]) == answers[i]);
    }

    return 0;
}
