#include <iostream>
#include <vector>
#include <unordered_map>

#include "../0000_utils/io.h"

// Solutin 1 
// Time complexity O(n2)
// Space complexity O(1)
std::vector<int> twoSum(const std::vector<int>& nums, int target) {
    std::vector<int> result;
    // If you haven't found the answers in the first run
    for (int i = 0; i < nums.size(); ++i) {
        for (int j = i + 1; j < nums.size(); ++j) {
            if (nums[i] + nums[j] == target) {
                       
            result.push_back(i);
            result.push_back(j);
            return result;
            }
        }   
    }
        
    return result;
}

// Solutin 2 
// Time complexity O(n)
// Space complexity O(n)
std::vector<int> twoSum2(const std::vector<int>& nums, int target) {
    std::unordered_map<int,int> myMap;

    for (int i = 0; i < nums.size(); ++i) {
        myMap[nums[i]] = i;
    }

    for (int i = 0; i < nums.size(); ++i) {

        int complement = target - nums[i];
        auto search = myMap.find(complement);

        if (search != myMap.end() && search->second != i) {
            return std::vector<int> {i, search->second};

        }
    }

    return std::vector<int>();
}

int main () {

    // Get the inputs example inputs
    std::vector<std::vector<int>> inputs {
        { 2, 7, 11, 15 },
        { 3, 2, 4 },
        { 3, 3 }
    };

    std::vector<int> targets = { 9, 6, 6 };

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        printVector(inputs[i]);
        std::vector<int> result {twoSum2(inputs[i], targets[i])};
        printVector(result);
        std::cout << "\n";
    }

    return 0;
}
