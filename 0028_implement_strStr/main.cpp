#include <iostream>
#include <string>
#include <vector>

#include "../0000_utils/io.h"

// Simpler
int strStr1(std::string haystack, std::string needle) {

    if (needle.empty()) 
        return 0;

    int hSize = haystack.size();
    int nSize = needle.size();

    if (hSize < nSize)
        return -1;

    for (int i = 0; i < hSize - nSize + 1; ++i) {
        for (int j = 0; j < nSize; ++j) {
            if (haystack[j+i] != needle[j])
                break;

            if (j + 1== nSize)
                return i;
        }
    }
    
    return -1;
 }

 // opitmized
int strStr2(std::string haystack, std::string needle) {

    if (needle.empty()) 
        return 0;

    if (!needle.empty() && haystack.empty())
        return -1;

    int hSize = haystack.size();
    int nSize = needle.size();

    for (int i = 0; i < hSize; ++i) {

        if (haystack[i] == needle[0]) {

            if (hSize - i < nSize) {
                return -1;
            } else if (nSize == 1) {
                return i;
            } else {

                for (int i2 = i + 1, j = 1; j < nSize; ++j) {

                    if (i2 != hSize) {
                        
                        if (haystack[i2] != needle[j]) {
                            break;
                        }
                    }

                    if (j + 1 == nSize) {
                        return i;
                    }

                    ++i2;
                }
            }
        }
    }
    
    return -1;
}


int main () {

    // Get the inputs example inputs
    std::vector<std::string> inputs {
        "hello",
        "aaaaa",
        "",
        "",
        "a",
        "a",
        "ab",
        "abc",
        "mississippi",
        "mississippi"
    };

        std::vector<std::string> needles {
        "ll",
        "bba",
        "",
        "a",
        "a",
        "aa",
        "ab",
        "abc",
        "mississippi",
        "issip"
    };

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        std::cout << "str: " << inputs[i] << "\nneedle: " << needles[i] << '\n';
        std::cout << strStr1(inputs[i], needles[i] ) << strStr2(inputs[i], needles[i] )
                  << "\n";
    }

    return 0;
}
