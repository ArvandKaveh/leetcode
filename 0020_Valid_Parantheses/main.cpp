#include <iostream>
#include <string>
#include <vector>
#include <ios>
#include <map>
#include "../0000_utils/io.h"


bool isValid(std::string s) {
    
    std::map<char, char> pairs  = {
        {'(', ')'},
        {'{', '}'},
        {'[', ']'},
    };

    if (s.empty())
        return true;

    std::string NCP; // None Closed Paranthese

    for (const auto &c : s) {
        if ( (c == ')' || c == '}' || c == ']')) {
            if (NCP.size() == 0 || c != pairs[NCP.back()])
                return false;
            else
                NCP.pop_back();
        }
        if (c == '(' || c == '{' || c == '[') {
            NCP += c;
        }
    }

    if (NCP.size() == 0)
        return true;
    
    return false;
 }


int main () {

    // Get the inputs example inputs
    std::vector<std::string> inputs {
        "()",
        "()[]{}",
        "(]",
        "([)]",
        "{[]}"
    };

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        printVector(inputs);
        std::cout << std::boolalpha
                  << isValid(inputs[i])
                  << "\n";
    }

    return 0;
}
