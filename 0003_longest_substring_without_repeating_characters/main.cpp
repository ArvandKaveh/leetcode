#include <iostream>
#include <vector>
#include <cassert>
#include <set>
#include <algorithm>

#include "../0000_utils/io.h"

// O(logN): N being the size of the string
int lengthOfLongestSubstring(std::string s) {
    
    if (s.empty())
        return 0;
        
    std::set<char> cache;
    int max = 0, i = 0, j = 0;
    do {
        if (cache.count(s[j]) != 0) {
            cache.erase(s[i]);
            i++;
        } else {
            cache.insert(s[j]);
            j++;
            max = (cache.size() > max) ? cache.size() : max;
        }
    } while (j < s.size());

    return max;
}


int main () {

    // Get the inputs example inputs
    std::vector<std::string> inputs {
        "abcabcbb",
        "bbbbb",
        "pwwkew",
        "",
        " ",
        "aab",
        "dvdf"
    };


    int answers[] {3, 1, 3, 0, 1, 2, 3};

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        int answer = lengthOfLongestSubstring(inputs[i]);
        std::cout << inputs[i] << ": " << answer
                  << '\n';

    assert(answer == answers[i]);
    }

    return 0;
}
