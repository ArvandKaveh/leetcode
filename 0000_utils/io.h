#ifndef _UTILS_IO_H
#define _UTILS_IO_H

#include <iostream>
#include <sstream>
#include <vector>
#include <set>


template <class T>
void printVector(const std::vector<T> &vec, std::ostream &os=std::cout) {
  os << "[";
  for (auto it = vec.begin(); it != vec.end(); ++it) {

    auto nx = std::next(it);
    if (nx != vec.end())
      os << *it << ",";
    else
      os << *it;
  }
  os << "]\n";
}

template <class T>
void printSet(const std::set<T> &s, std::ostream &os=std::cout) {
  os << "[";
  for (auto it = s.begin(); it != s.end(); ++it) {

    auto nx = std::next(it);
    if (nx != s.end())
      os << *it << ",";
    else
      os << *it;
  }
  os << "]\n";
}

/*** Print Functions ***/


#endif // _UTILS_IO_H