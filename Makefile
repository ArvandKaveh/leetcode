DIRECTORIES = $(wildcard */)

.PHONY: all
all:
	for f in $(DIRECTORIES); do $(MAKE) -C $$f; done

.PHONY: test
test: all
	-for f in $(DIRECTORIES); do $(MAKE) -C $$f test; done