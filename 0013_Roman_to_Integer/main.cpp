#include <iostream>
#include <string>
#include <vector>
#include <ios>
#include <map>
#include <string_view>
#include "../0000_utils/io.h"

std::map<std::string, int> myMap {
    {"I", 1},
    {"IV", 4},
    {"V", 5},
    {"IX", 9},
    {"X", 10},
    {"XL", 40},
    {"L", 50},
    {"XC", 90},
    {"C", 100},
    {"CD", 400},
    {"D", 500},
    {"CM", 900},
    {"M", 1000},
};

// Solution1
// O(S): S being the size of the intupt string 
 int romanToInt(std::string s) {
    int num = 0;
    for (int i = 0; i < s.size(); ++i) {

         if ( i + 1 < s.size()) {
            auto it = myMap.find(s.substr(i,2));
            if (it != myMap.end()) {
                num += it->second;
                ++i;
            } else {
                num += myMap[s.substr(i,1)];
            }
        } else {
        num += myMap[s.substr(i,1)];
        }
    }

    return num;
 }


int main () {

    // Get the inputs example inputs
    std::vector<std::string> inputs {
        "III",
        "IV",
        "IX",
        "LVIII",
        "MCMXCIV"
    };

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        printVector(inputs);
        std::cout << std::boolalpha
                  << romanToInt(inputs[i])
                  << "\n";
    }

    return 0;
}
