#include <iostream>
#include <vector>
#include <cassert>
#include "../0000_utils/io.h"


void addOne(int& digit, bool& remainderFlag) {
        if (digit + 1 == 10) {
            digit = 0;
            remainderFlag = true;
        } else {
            ++digit;
            remainderFlag = false;
        }
}

// Solution1
std::vector<int> plusOne(std::vector<int>& digits) {
    
    bool remainderFlag = false;
    addOne(digits[digits.size() - 1], remainderFlag);

    for (int i = digits.size() - 2; i >= 0; --i) {
        if (remainderFlag) {
            addOne(digits[i], remainderFlag);
        }
    }

    if (digits[0] == 0) {
        digits.resize(digits.size() + 1);

        for (int i = digits.size() - 2; i >= 0; --i) {
            digits[i+1] = digits[i];
        }

        digits[0] = 1;
    }

    return digits;
 }


int main () {

    // Get the inputs example inputs
    std::vector<std::vector<int>> inputs {
        {4,3,2,1},
        {0},
        {1,2,3},
        {9},
        {9,9},
        {1,9,9}
    };

    std::vector<std::vector<int>> answers {
        {4,3,2,1},
        {1},
        {1,2,4},
        {1,0},
        {1,0,0},
        {2,0,0}
    };

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        printVector(inputs[i]);
        std::cout << ": ";
        printVector(plusOne(inputs[i]));
        std::cout << '\n';

    //assert(plusOne(inputs[i]) == answers[i]);
    }

    return 0;
}
