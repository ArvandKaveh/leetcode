#include <iostream>
#include <string>
#include <vector>
#include "../0000_utils/io.h"

int getMinStrSize(const std::vector<std::string>& strs) {
    int min = strs[0].size();

    for (int i = 1; i < strs.size(); ++i) {
        if (strs[i].size() < min)
            min = strs[i].size();
    }
    
    std::cout << "min: " << min << "\n";
    return min;
}

bool checkAllStrsAtIndex (const std::vector<std::string>& strs, int index) {
    bool allSame = false;
    char c = strs[0][index];
    for (const auto& s : strs ) {
        if (s[index] != c)
            return false;
    }

return true;
}

std::string longestCommonPrefix(const std::vector<std::string>& strs) {
    int strsSize = strs.size();
    int minStrSize = getMinStrSize(strs);

    int commonPrefLength = 0;
    for (int i = 0; i < minStrSize; ++i) {
        if(checkAllStrsAtIndex(strs, i)) {
            ++commonPrefLength;
        } else {
            break;
        }
    }

    if (commonPrefLength > 0){
        return strs[0].substr(0, commonPrefLength);
    }
    else {
        return "";
    }
}

std::string LCP_Opt(const std::vector<std::string>& strs) {
    if (strs.empty()) {
        return "";
    }

    for (int i = 0; i < strs[0].size(); ++i) {
        
        char c = strs[0][i];
        for (int j = 1; j < strs.size(); ++j)
            if ( i == strs[j].size() || strs[j][i] != c )
                return strs[0].substr(0, i);
    }

    return strs[0];
}

int main () {

    // Get the inputs example inputs
    std::vector<std::vector<std::string>> inputs {
        { "flower","flow","flight"},
        { "dog","racecar","car" },
        {"a"}
    };

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        printVector(inputs[i]);
        std::vector<std::string> result {LCP_Opt(inputs[i])};
        printVector(result);
        std::cout << "\n";
    }

    return 0;
}
