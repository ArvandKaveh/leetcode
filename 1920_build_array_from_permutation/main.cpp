#include <iostream>
#include <vector>
#include <unordered_map>

#include "../0000_utils/io.h"

// Solutin 1
// Time complexity O(#)
// Space complexity O(n#)
std::vector<int> buildArray(const std::vector<int>& nums) {
        std::vector<int> res;
        for (int i = 0; i < nums.size(); ++i){
            res.push_back(nums[nums[i]]);
        }
        return res;
}

int main () {

    // Get the inputs example inputs
    std::vector<std::vector<int>> inputs {
        { 0,2,1,5,3,4 },
        { 5,0,1,2,3,4 },
    };

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        printVector(inputs[i]);
        std::vector<int> result {buildArray(inputs[i])};
        printVector(result);
        std::cout << "\n";
    }

    return 0;
}
