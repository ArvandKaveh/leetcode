#include <iostream>
#include <vector>

#include "../0000_utils/io.h"

// O(N): N being the size of the input array
// omega(N): N being the size of the input array
int removeDuplicates(std::vector<int>& nums) {

    if (nums.empty()) return 0;
    
    int k = 1;
    for (int i = 1; i < nums.size(); ++i) {
        if (nums[i] > nums[k - 1]) {
            nums[k++] = nums[i];
        }
    }
    
    return k;
 }


int main () {

    // Get the inputs example inputs
    std::vector<std::vector<int>> inputs {
        {1,1,2},
        {0,0,1,1,1,2,2,3,3,4},
        {1},
        {-1,2},
        {2,-1},
        {1,1}
    };

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {


        printVector(inputs[i]);
        std::cout << removeDuplicates(inputs[i])
                  << "\n";
        printVector(inputs[i]);
    }

    return 0;
}
