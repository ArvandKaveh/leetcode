#include <iostream>
#include <vector>

#include "../0000_utils/io.h"

// O(N): N being the size of the input array
// omega(N): N being the size of the input array
int removeElement(std::vector<int>& nums, int val) {

    if (nums.empty()) 
        return 0;

    int k = 0;
    for (int i = 0; i < nums.size(); ++i) {
        if (nums[i] != val) {
            nums[k++] = nums[i];
        }
    }

    return k;
 }


int main () {

    // Get the inputs example inputs
    std::vector<std::vector<int>> inputs {
        {3,2,2,3},
        {0,1,2,2,3,0,4,2}
    };

    int val[] {3, 2};

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {


        printVector(inputs[i]);
        std::cout << removeElement(inputs[i], val[i])
                  << "\n";
        printVector(inputs[i]);
    }

    return 0;
}
