#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <cassert>

std::vector<int> runningSum(const std::vector<int>& nums) {
    std::vector<int> result = {nums[0]};

    for (int i = 0; i < nums.size(); ++i) {
        result.push_back(result.back() + nums[i]);
    }

    return result;
}

template <class T>
void printVector(const std::vector<T>& vec, std::ostream& os = std::cout) {
    os << "[";
    for (auto it = vec.begin(); it!=vec.end(); ++it) {

        auto nx = std::next(it);
        if (nx != vec.end())
            os << *it << ",";
        else 
            os << *it;
    }
    os << "]";
}

int main () {

    std::vector<int> nums1 = {1, 2, 3, 4};
    std::vector<int> nums2 = {1, 1, 1, 1, 1};
    std::vector<int> nums3 = {3, 1, 2, 10, 1};

    std::vector<int> answer1{runningSum(nums1)};
    std::vector<int> answer2{runningSum(nums2)};
    std::vector<int> answer3{runningSum(nums3)};

    printVector(nums1);
    printVector(answer1);
    std::cout << "\n";

    printVector(nums2);
    printVector(answer2);
    std::cout << "\n";

    printVector(nums3);
    printVector(answer3);
    std::cout << "\n";


    // Test 
    std::vector<int> test_int1 { 1, 2, 3 };
    std::ostringstream op_test_int1;
    printVector(test_int1, op_test_int1);
    assert(op_test_int1.str() == "[1,2,3]");


    // std::cout << op_test_int1.str() << "\n";

    return 0;
}
