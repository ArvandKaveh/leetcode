#include <iostream>
#include <vector>
#include <cassert>

#include "../0000_utils/io.h"

std::vector<int> distinctWays(46, 0);

// The answer seems to follow this pattern:
// N(1) = 1, N(2) = 2
// N(n+1) = N(n) + N(n-1), where n is the number of steps and n > 2
int climbStairs(int n) {

    if (distinctWays[n] != 0) {
        return distinctWays[n];
    }

    if (n < 3) {
        distinctWays[n] = n;
        return n;
    }

    int x = climbStairs(n - 1) + climbStairs(n - 2);
    distinctWays[n] = x;
    return x;
 }


int main () {

    // Get the inputs example inputs
    std::vector<int> inputs {2, 3, 4, 5, 6};
    int answers[] {2, 3, 5, 8, 13};

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        int answer = climbStairs(inputs[i]);
        std::cout << inputs[i] << ": " << answer
                  << '\n';

    assert(answer == answers[i]);
    }

    return 0;
}
