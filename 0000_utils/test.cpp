#include "io.h"
#include "test.h"
#include "vector"
#include "cassert"
#include "sstream"

/*** Test Print Functions ***/

void testPrintVector() {
    
    // Prepare
    std::vector<int> test_int0;
    std::ostringstream op_test_int0;

    std::vector<int> test_int1 { 1, 2, 3 };
    std::ostringstream op_test_int1;
    
    // Act
    printVector<int>(test_int0, op_test_int0);
    printVector<int>(test_int1, op_test_int1);

    // Assert
    assert(op_test_int0.str() == "[]");
    assert(op_test_int1.str() == "[1,2,3]");
}