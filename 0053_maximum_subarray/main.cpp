#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "../0000_utils/io.h"

// Solution 1 - Brute force (sub-optimal solution)
// O(N2) : N being the size of the given array
int maxSubArray(std::vector<int>& nums) {
    if (nums.empty())
        return 0;

    int max = nums[0];
    for (int i = 0; i < nums.size(); ++i) {


        int temp = nums[i];
        if (temp > max) {
            max = temp;
        }
        for (int j = i + 1; j < nums.size(); ++j) {
            
            temp += nums[j];
            if (temp > max) {
                max = temp;
            }
        }   
    }

    return max;
}
// Solution 2 - Kadane's Algorithm
// O(N) : N being the size of the given array  
int Kadane(std::vector<int>& nums) {

    int max_current = nums[0];
    int max_global = nums[0];

    for (int i = 1; i < nums.size(); ++i) {

        max_current = std::max(nums[i], nums[i]+max_current);
        max_global = std::max(max_global, max_current);
    }

    return max_global;
}

int main () {

    // Get the inputs example inputs
    std::vector<std::vector<int>> inputs {
        {-2,1,-3,4,-1,2,1,-5,4},        
        {1},
        {5,4,-1,7,8},
        {},
        {-1,1},
        {-2,1}
    };

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        printVector(inputs[i]);
        std::cout << Kadane(inputs[i])
                  << "\n";
    }

    return 0;
}
