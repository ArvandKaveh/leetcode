#include <iostream>
#include <vector>
#include <cassert>

#include "../0000_utils/io.h"

//// the Recursive solution causes heap buffer overflow
////need to be re-written as while loop
// void findInsertPosition(std::vector<int>& nums, int from, int to, int target, int& pos) {

//         int mid = from + ((to - from) / 2);

//         if (from == to) {

//             // Base Case
//             if ( target < nums[from]) {
//                 pos = from;
//             } else {
//                 pos = from + 1;
//             }

//         } else if (target < nums[mid]) {
//             findInsertPosition(nums, from, mid, target, pos);
//         } else {
//             findInsertPosition(nums, mid + 1, to, target, pos);
//         }
// }

//  solution
int searchInsert(std::vector<int>& nums, int target) {

    int beg = 0;
    int end = nums.size() - 1;
    int mid = (nums.size() / 2);

    while (true) {
        //std::cout << "beg: " << beg << ", mid: " << mid << ", end: " << end << ", pos: " << pos << '\n';

        // base case
        if (beg == end) {
            if (target <= nums[beg]) {
                return beg;
            } else {
                return beg + 1;
            }

        // leads to base case
        } else if (target <= nums[mid]) {
            end = mid;
            mid = (end - beg) / 2;
        }  else {
            beg = mid + 1;
            mid = (end - beg) / 2;
        }
    }

    return -1;
 }


int main () {

    // Get the inputs example inputs
    std::vector<std::vector<int>> inputs {
        {1,3,5,6},
        {1,3,5,6},
        {1,3,5,6},
        {1,3,5,6},
        {1,3,5,6},
        {1},
        {1}
    };

    int target[] {5, 2, 7, 0, 3, 0, 2};
    int expectedResults[] {2, 1, 4, 0, 1, 0, 1};

    // Test the solution with the examples
    for (size_t i = 0; i < inputs.size(); ++i) {

        printVector(inputs[i]);
        std::cout << "target: " << target[i]
                  << ", output: " << searchInsert(inputs[i], target[i])
                  << "\n";
        assert(searchInsert(inputs[i], target[i]) == expectedResults[i]);
    }

    return 0;
}
